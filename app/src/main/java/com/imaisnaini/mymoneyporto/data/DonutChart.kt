package com.imaisnaini.mymoneyporto.data

data class DonutChart (
    var label: String,
    var percentage: Float,
    var transactions: List<Transaction>
)