package com.imaisnaini.mymoneyporto.data

import java.time.LocalDate

data class Transaction(
    var trx_date: LocalDate,
    var nominal: Double
)
