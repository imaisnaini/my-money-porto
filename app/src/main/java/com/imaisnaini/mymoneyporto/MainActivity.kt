package com.imaisnaini.mymoneyporto

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.rememberNavController
import com.imaisnaini.mymoneyporto.data.DonutChart
import com.imaisnaini.mymoneyporto.ui.DonutChart
import com.imaisnaini.mymoneyporto.ui.components.BarChart
import com.imaisnaini.mymoneyporto.ui.theme.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter


class MainActivity : ComponentActivity() {
    val mainViewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            mainViewModel.generateTransaction()
            mainViewModel.generateDonutChart()
            mainViewModel.generateLineChart()
            val navController = rememberNavController()
            BNIAppTheme() {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = Orange_700
                ) {
                    var donutChart = remember {
                        mutableStateListOf<DonutChart>()
                    }
                    var percentages = remember {
                        mutableStateListOf<Float>()
                    }
                    mainViewModel.getDonutChartLiveData().observe(this){
                        if (!it.isNullOrEmpty()){
                            donutChart.apply {
                                clear()
                                addAll(it)
                                percentages.apply {
                                    clear()
                                    addAll(it.map { it.percentage })
                                }
                            }
                        }
                    }

                    var barValues = remember {
                        mutableStateListOf<Float>()
                    }
                    mainViewModel.getLineChartLiveData().observe(this){
                        if (!it.isNullOrEmpty()){
                            barValues.apply {
                                clear()
                                addAll(it)
                            }
                        }
                    }
                    Column(modifier = Modifier
                        .fillMaxSize()
                        .background(Orange_700)) {
                        TopBar()
                        Card(
                            shape = RoundedCornerShape(topStart = radius_xlarge, topEnd = radius_xlarge),
                            modifier = Modifier
                                .fillMaxSize()
                        ) {
                            Column() {
                                Greeting(name = stringResource(id = R.string.dev_name))
                                Spacer(modifier = Modifier.height(padding_normal))
                                Row(modifier = Modifier.background(Grey_100)) {
                                    Text(text = "Tracker 2023", style = Typography.subtitle2,
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .padding(all = padding_large))
                                }
                                Column(modifier = Modifier.padding(horizontal = padding_normal)) {
                                    BarsChart(barValues)
                                    Spacer(modifier = Modifier.height(padding_normal))
                                    DoughnutChart(donutChart, percentages)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    @Composable
    fun Greeting(name: String) {
        Text(text = "Hello $name", style = Typography.subtitle1,
            modifier = Modifier.padding(horizontal = padding_large, vertical = padding_normal))
    }

    @Composable
    private fun TopBar(){
        TopAppBar (
            title = { Text(stringResource(R.string.app_name)) },
            elevation = 0.dp,
        )
    }

    @Composable
    fun DoughnutChart(donutChart: List<DonutChart>, percentages: List<Float>){
        val activity = LocalContext.current as Activity
        Card(shape = Shapes.small, backgroundColor = Turquoise_100,
            modifier = Modifier.fillMaxWidth()) {
            Row(modifier = Modifier.padding(all = padding_normal)) {
                if (percentages.size.equals(chartColor.size)) {
                    DonutChart(
                        colors = chartColor,
                        inputValues = percentages,
                        inputLabel = donutChart.map { it.label },
                        modifier = Modifier.size(img_large)
                    )
                }
                Column(horizontalAlignment = Alignment.End, verticalArrangement = Arrangement.Center,
                    modifier = Modifier.fillMaxWidth()) {
                    LazyColumn(){
                        itemsIndexed(donutChart){index, item ->
                            Card(shape = Shapes.large, backgroundColor = chartColor[index],
                                modifier = Modifier
                                    .clickable {
                                        val bundle = Bundle()
                                        bundle.putInt("index", index)
                                        activity.startActivity(
                                            Intent(this@MainActivity, SecondActivity::class.java)
                                                .putExtras(bundle)
                                        )
                                    }
                                    .padding(vertical = padding_small)
                            ) {
                                Text(text = item.label, style = Typography.caption,
                                    modifier = Modifier
                                        .padding(horizontal = padding_large, vertical = padding_small)
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    @Composable
    fun BarsChart(barValues: List<Float>){
        Card(shape = Shapes.small, backgroundColor = Grey_100,
            modifier = Modifier.fillMaxWidth()) {
            if (!barValues.isNullOrEmpty()){
                BarChart(values = barValues, maxHeight = img_medium)
            }
        }
    }
}