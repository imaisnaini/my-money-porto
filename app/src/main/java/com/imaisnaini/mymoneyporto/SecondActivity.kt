package com.imaisnaini.mymoneyporto

import android.content.SharedPreferences
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ChevronLeft
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHost
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.imaisnaini.mymoneyporto.data.DonutChart
import com.imaisnaini.mymoneyporto.data.Transaction
import com.imaisnaini.mymoneyporto.ui.theme.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter


class SecondActivity : ComponentActivity() {
    val mainViewModel: MainViewModel by viewModels()
    var index = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        index = intent.getIntExtra("index", 0)
        setContent {
            mainViewModel.generateTransaction()
            mainViewModel.generateDonutChart()
            mainViewModel.generateLineChart()
            val navController = rememberNavController()
            BNIAppTheme() {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                ) {
                    var donutChart = remember {
                        mutableStateListOf<DonutChart>()
                    }
                    mainViewModel.getDonutChartLiveData().observe(this){
                        if (!it.isNullOrEmpty()){
                            donutChart.apply {
                                clear()
                                addAll(it)
                            }
                        }
                    }
                    Column(modifier = Modifier
                        .fillMaxSize()) {
                        if (!donutChart.isNullOrEmpty()) {
                            TopBar(donutChart.get(index).label)
                            LazyColumn() {
                                items(donutChart[index].transactions) {
                                    TransactionItem(data = it)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    @Composable
    private fun TopBar(title: String){
        TopAppBar (
            title = { Text(title) },
            elevation = 0.dp,
            navigationIcon = {
                IconButton(onClick = { this.finish() }) {
                    Icon(imageVector = Icons.Filled.ChevronLeft, "",
                        modifier = Modifier.size(icon_medium))
                }
            }
        )
    }

    @Composable
    fun TransactionItem(data: Transaction){
        Column(modifier = Modifier.fillMaxWidth()) {
            Row(verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = padding_normal, vertical = padding_large)) {
                Image(
                    painterResource(id = R.drawable.ic_dollar_symbol), contentDescription = "Dollar Icon",
                    modifier = Modifier.size(icon_medium))
                Column(modifier = Modifier
                    .weight(1f, true)
                    .padding(start = padding_normal)) {
                    Text(text = data.trx_date.toString(), color = Grey_700,)
                }
                Text(text = "RP ${data.nominal}", fontWeight = FontWeight.Bold)
            }
            Divider()
        }
    }
}