package com.imaisnaini.mymoneyporto

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.imaisnaini.mymoneyporto.data.DonutChart
import com.imaisnaini.mymoneyporto.data.Transaction
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Locale

class MainViewModel : ViewModel() {
    private var donutChartLiveData = MutableLiveData<List<DonutChart>>()
    fun getDonutChartLiveData() = donutChartLiveData

    private var lineChartLiveData = MutableLiveData<List<Float>>()
    fun getLineChartLiveData() = lineChartLiveData

    private var transactionLiveData = MutableLiveData<List<Transaction>>()
    fun getTransactionLiveData() = transactionLiveData

    fun generateTransaction(){
        val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.getDefault())
        val trx1 = Transaction(LocalDate.parse("21/01/2023", formatter), 1000000.0)
        val trx2 = Transaction(LocalDate.parse("20/01/2023", formatter), 500000.0)
        val trx3 = Transaction(LocalDate.parse("19/01/2023", formatter), 1000000.0)
        val trx4 = Transaction(LocalDate.parse("21/01/2023", formatter), 159000.0)
        val trx5 = Transaction(LocalDate.parse("20/01/2023", formatter), 35000.0)
        val trx6 = Transaction(LocalDate.parse("19/01/2023", formatter), 1500.0)
        val trx7 = Transaction(LocalDate.parse("21/01/2023", formatter), 200000.0)
        val trx8 = Transaction(LocalDate.parse("20/01/2023", formatter), 195000.0)
        val trx9 = Transaction(LocalDate.parse("19/01/2023", formatter), 5000000.0)
        val trx10 = Transaction(LocalDate.parse("21/01/2023", formatter), 1000000.0)
        val trx11 = Transaction(LocalDate.parse("20/01/2023", formatter), 500000.0)
        val trx12 = Transaction(LocalDate.parse("19/01/2023", formatter), 1000000.0)

        transactionLiveData.value = listOf(trx1, trx2, trx3, trx4, trx5, trx6, trx7, trx8, trx9, trx10, trx11, trx12)
    }

    fun generateDonutChart(){
        if (transactionLiveData.value.isNullOrEmpty()){
            generateTransaction()
        }
        val partition: List<List<Transaction>> = transactionLiveData.value!!.chunked(3)
        var data1 = DonutChart("Tarik Tunai", 55f, partition[0])
        var data2 = DonutChart("QRIS Payment", 31f, partition[1])
        var data3 = DonutChart("Topup Gopay", 7.7f, partition[2])
        var data4 = DonutChart("Lainnya", 6.3f, partition[3])
        donutChartLiveData.value = listOf(data1, data2, data3, data4)
    }

    fun generateLineChart(){
        lineChartLiveData.value = listOf(3f, 7f, 8f, 10f, 5f, 10f, 1f, 3f, 5f, 10f, 7f, 7f)
    }
}